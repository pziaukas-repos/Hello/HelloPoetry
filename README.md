# Hello Python Poetry

Python Poetry demo project, based on https://poetry.eustace.io/docs.


## Dependency management

### Installation
```
pip install poetry
poetry install
```

### Execution
```
poetry run pytest
```
